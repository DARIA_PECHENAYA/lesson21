const path = require('path');
const {HtmlWebpackPlugin} = require('html-webpack-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
module.exports = {
    content: path.resolve(__dirname,'src')
    mode: 'development',
    entry: {
        main: './index.js',
        helper: './helper.js'
    },
    output: {
        filename: '[name].[contenthash].bundle.js',
        path: path.resolve(__dirname, 'dist')
    },
    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            template: './src/index.html'
        })
    ],
    module: {
        rules[
            {
                test: /\.(jpeg|svg|jpg|gif)$/,
                use:['file-loader']
            }
        ]
    }
};